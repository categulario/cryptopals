use std::collections::HashMap;

pub mod utilities;
pub mod ecb;
pub mod cbc;
pub mod ctr;
pub mod repeating_key_xor;
pub mod mt19937;

lazy_static! {
    /// Frequency of letters in english by percentage.
    pub static ref LETTER_FREQUENCY: HashMap<char, f64> = vec![
        ('e', 11.162/100.0),
        ('t', 9.356/100.0),
        ('a', 8.497/100.0),
        ('r', 7.587/100.0),
        ('i', 7.546/100.0),
        ('o', 7.507/100.0),
        ('n', 6.749/100.0),
        ('s', 6.327/100.0),
        ('h', 6.094/100.0),
        ('d', 4.253/100.0),
        ('l', 4.025/100.0),
        ('u', 2.758/100.0),
        ('w', 2.560/100.0),
        ('m', 2.406/100.0),
        ('f', 2.228/100.0),
        ('c', 2.202/100.0),
        ('g', 2.015/100.0),
        ('y', 1.994/100.0),
        ('p', 1.929/100.0),
        ('b', 1.492/100.0),
        ('k', 1.292/100.0),
        ('v', 0.978/100.0),
        ('j', 0.153/100.0),
        ('x', 0.150/100.0),
        ('q', 0.095/100.0),
        ('z', 0.077/100.0),
    ].into_iter().collect();
}
