use std::env;
use std::fs::File;
use std::io::Read;

use cryptopals::repeating_key_encrypt;
use cryptopals::encoding::b64;

fn main() -> std::io::Result<()> {
    let args: Vec<_> = env::args().collect();
    let data_filename = args.get(1).expect("need a data file name");
    let key = args.get(2).expect("need a key");

    let mut data = String::new();

    let mut file = File::open(data_filename)?;
    file.read_to_string(&mut data)?;

    let encrypted_bytes = repeating_key_encrypt(data.as_bytes(), key.as_bytes());

    println!("{}", b64::encode(&encrypted_bytes));

    Ok(())
}
