use std::env;
use std::io::{self, BufReader, BufRead};
use std::fs::File;
use std::collections::HashSet;

const BLOCK_SIZE: usize = 32;

fn detect(line: &[u8]) -> u32 {
    let mut data = HashSet::new();
    let mut repeated = 0;

    for b in 0..line.len()/BLOCK_SIZE {
        let block = &line[b*BLOCK_SIZE..(b+1)*BLOCK_SIZE];

        if data.contains(block) {
            repeated += 1;
        }

        data.insert(block);
    }

    repeated
}

fn printblocks(line: &[u8]) {
    for b in 0..line.len()/BLOCK_SIZE {
        let block = &line[b*BLOCK_SIZE..(b+1)*BLOCK_SIZE];
        println!("{}", String::from_utf8_lossy(block));
    }
}

fn main() -> io::Result<()> {
    let args: Vec<_> = env::args().collect();
    let f = File::open(args.get(1).expect("need an argument"))?;
    let reader = BufReader::new(f);

    for (index, line) in reader.lines().enumerate() {
        let line = line?;
        let bytes = line.trim().as_bytes();
        let repeated = detect(bytes);

        if repeated > 0 {
            println!("repeticion {} en la linea {}", repeated, index);
            printblocks(bytes);
        }
    }

    Ok(())
}
