use cryptopals::{
    encoding::hex, single_byte_xor_decode,
    breaking::{
        repeating_key_xor::guess_single_byte_xor_key,
        LETTER_FREQUENCY,
    },
};

fn main() {
    let hex_message = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
    let bytes = hex::decode(hex_message);

    let (key, error) = guess_single_byte_xor_key(&LETTER_FREQUENCY, &bytes);
    let decoded_bytes = single_byte_xor_decode(&bytes, key);

    let decoded_message = String::from_utf8_lossy(&decoded_bytes);

    println!("Found key {} with error {}", key as char, error);

    assert_eq!(decoded_message, "Cooking MC's like a pound of bacon");

    println!("Message: {}", decoded_message);
}
