use std::env;
use std::fs::File;
use std::io::{BufReader, BufRead};

use cryptopals::{
    encoding::hex, breaking::{
        LETTER_FREQUENCY, repeating_key_xor::guess_single_byte_xor_key,
    },
    single_byte_xor_decode,
};

/// Detects single-byte XOR
fn main() -> std::io::Result<()> {
    let args: Vec<_> = env::args().collect();
    let filename = args.get(1).expect("Need a file argument");
    let f = File::open(filename)?;
    let reader = BufReader::new(f);

    let mut rated_lines: Vec<_> = reader
        .lines()
        .filter_map(|l| l.ok())
        .map(|l| {
            let bytes = hex::decode(&l);
            let (key, error) = guess_single_byte_xor_key(&LETTER_FREQUENCY, &bytes);

            (error, l, bytes, key)
        })
        .collect();

    rated_lines.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());

    let (error, string, bytes, key) = &rated_lines[0];

    println!("Error: {} String: {} Key: {}", error, string, *key as char);

    let decoded_message = single_byte_xor_decode(&bytes, *key);
    let decoded_message = String::from_utf8_lossy(&decoded_message);

    println!("Decoded message: {}", decoded_message);

    Ok(())
}
