use std::collections::HashSet;
use std::time::SystemTime;

use cryptopals::random::mt19937::Generator;

fn main() {
    let now = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs();
    let mut outputs = HashSet::new();

    for i in 0..2000 {
        outputs.insert(Generator::init_genrand(now - i).next().unwrap());
    }

    println!("{}", outputs.len());
}
