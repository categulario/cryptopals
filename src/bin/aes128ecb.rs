use std::env;
use std::fs::File;
use std::io::{Read, Write};

use cryptopals::encoding::b64;
use cryptopals::crypto::rijndael;

fn get_key() -> std::io::Result<Vec<u8>> {
    eprint!("key: ");
    std::io::stderr().flush()?;
    let mut key = String::new();
    std::io::stdin().read_line(&mut key)?;
    Ok(key.trim().bytes().collect())
}

fn main() -> std::io::Result<()> {
    let args: Vec<_> = env::args().collect();
    let action = args.get(1).expect("Need an argument, -h for help");

    if action == "-h" || action == "--help" {
        println!("Usage: aes128ecb [encrypt|decrypt] filename");
        return Ok(());
    } else if action != "encrypt" && action != "decrypt" {
        println!("First argument must be either 'encrypt' or 'decrypt'");
        return Ok(());
    }

    let filename = args.get(2).expect("needs a filename as second argument, pass -h for help");

    match action.as_str() {
        "encrypt" => {
            let mut data = Vec::new();
            let mut file = File::open(filename)?;
            file.read_to_end(&mut data)?;

            let key = get_key()?;

            println!("{}", b64::encode(&rijndael::ecb::encrypt(&data, &key)));
        }
        "decrypt" => {
            let mut data = String::new();
            let mut file = File::open(filename)?;

            file.read_to_string(&mut data)?;

            let clean_data = data.split("\n").map(|s| s.trim()).filter(|s| s.len() > 0).collect::<Vec<_>>().join("");

            let key = get_key()?;

            let decoded = b64::decode(&clean_data);
            println!("{}", String::from_utf8_lossy(&rijndael::ecb::decrypt(&decoded, &key)));
        }
        option => {
            println!("What do you mean by {}?", option);
        }
    }

    Ok(())
}
