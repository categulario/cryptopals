/* Period parameters */
const N: usize = 624;
const M: usize = 397;
const MATRIX_A: u64 = 0x9908b0df;   /* constant vector a */
const UPPER_MASK: u64 = 0x80000000; /* most significant w-r bits */
const LOWER_MASK: u64 = 0x7fffffff; /* least significant r bits */

pub struct Generator {
    mt: [u64; N], /* the array for the state vector  */
    mti: usize, /* mti==N+1 means mt[N] is not initialized */
}

impl Generator {
    pub fn new() -> Generator {
        Generator::init_genrand(5489) /* a default initial seed is used */
    }

    /* initializes mt[N] with a seed */
    pub fn init_genrand(s: u64) -> Generator {
        let mut mt = [0; N];

        mt[0]= s & 0xffffffff;

        for mti in 1..N {
            mt[mti] = 1812433253u64.wrapping_mul(mt[mti-1] ^ (mt[mti-1] >> 30)) + mti as u64;

            /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
            /* In the previous versions, MSBs of the seed affect   */
            /* only MSBs of the array mt[].                        */
            /* 2002/01/09 modified by Makoto Matsumoto             */
            mt[mti] &= 0xffffffff;
            /* for >32 bit machines */
        }

        Generator {
            mt,
            mti: 0,
        }
    }

    /* initialize by an array with array-length */
    /* init_key is the array for initializing keys */
    /* key_length is its length */
    /* slight change for C++, 2004/2/26 */
    pub fn init_by_array(init_key: &[u64]) -> Generator {
        let Generator {
            mti, mut mt
        } = Generator::init_genrand(19650218);

        let mut i = 1;
        let mut j = 0;

        for _ in (1..(N.max(init_key.len()) + 1)).rev() {
            mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)).wrapping_mul(1664525))) + init_key[j] + j as u64; /* non linear */
            mt[i] &= 0xffffffff; /* for WORDSIZE > 32 machines */
            i += 1;
            j += 1;

            if i>=N {
                mt[0] = mt[N-1]; i=1;
            }

            if j >= init_key.len() {
                j=0;
            }
        }

        for _ in (1..N).rev() {
            mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)).wrapping_mul(1566083941))) - i as u64; /* non linear */
            mt[i] &= 0xffffffff; /* for WORDSIZE > 32 machines */
            i += 1;

            if i>=N {
                mt[0] = mt[N-1]; i=1;
            }
        }

        mt[0] = 0x80000000; /* MSB is 1; assuring non-zero initial array */

        Generator {
            mt, mti,
        }
    }

    /* generates a random number on [0,0xffffffff]-interval */
    fn genrand_int32(&mut self) -> u64 {
        if self.mti >= N { /* generate N words at one time */
            let mag01 = [0x0, MATRIX_A];

            for kk in 0..(N-M) {
                let y = (self.mt[kk]&UPPER_MASK)|(self.mt[kk+1]&LOWER_MASK);
                self.mt[kk] = self.mt[kk+M] ^ (y >> 1) ^ mag01[(y & 0x1) as usize];
            }

            for kk in (N-M)..(N-1) {
                let y = (self.mt[kk]&UPPER_MASK)|(self.mt[kk+1]&LOWER_MASK);
                self.mt[kk] = self.mt[kk.overflowing_add(M.overflowing_sub(N).0).0] ^ (y >> 1) ^ mag01[(y & 0x1) as usize];
            }

            let y = (self.mt[N-1]&UPPER_MASK)|(self.mt[0]&LOWER_MASK);
            self.mt[N-1] = self.mt[M-1] ^ (y >> 1) ^ mag01[(y & 0x1) as usize];

            self.mti = 0;
        }

        let mut y = self.mt[self.mti];

        self.mti += 1;

        /* Tempering */
        y ^= y >> 11;
        y ^= (y << 7) & 0x9d2c5680;
        y ^= (y << 15) & 0xefc60000;
        y ^= y >> 18;

        return y;
    }

    /* generates a random number on [0,0x7fffffff]-interval */
    pub fn genrand_int31(&mut self) -> u64 {
        self.genrand_int32() >> 1
    }

    /* generates a random number on [0,1]-real-interval */
    pub fn genrand_real1(&mut self) -> f64 {
        self.genrand_int32() as f64 * (1.0/4294967295.0)
        /* divided by 2^32-1 */
    }

    /* generates a random number on [0,1)-real-interval */
    pub fn genrand_real2(&mut self) -> f64 {
        self.genrand_int32() as f64 * (1.0/4294967296.0)
        /* divided by 2^32 */
    }

    /* generates a random number on (0,1)-real-interval */
    pub fn genrand_real3(&mut self) -> f64 {
        ((self.genrand_int32() as f64) + 0.5)*(1.0/4294967296.0)
        /* divided by 2^32 */
    }

    /* generates a random number on [0,1) with 53-bit resolution*/
    pub fn genrand_res53(&mut self) -> f64 {
        let a = (self.genrand_int32() >> 5) as f64;
        let b = (self.genrand_int32() >> 6) as f64;

        (a*67108864.0+b)*(1.0/9007199254740992.0)
    }
    /* These real versions are due to Isaku Wada, 2002/01/09 added */
}

impl Iterator for Generator {
    type Item = u64;

    fn next(&mut self) -> Option<u64> {
        Some(self.genrand_int32())
    }
}

#[cfg(test)]
mod tests {
    use super::Generator;

    #[test]
    fn init_by_array() {
        let init = [0x123, 0x234, 0x345, 0x456];

        let gen = Generator::init_by_array(&init);

        let outputs = [2282758660, 3074130015, 3318876626, 1125713583, 2319579370];

        for (o, g) in outputs.iter().zip(gen) {
            assert_eq!(*o, g);
        }
    }

    #[test]
    fn init_by_integer() {
        let gen = Generator::init_genrand(60);

        let outputs = [184947965, 3675138408, 2563530752, 2423434164, 3536133400];

        for (o, g) in outputs.iter().zip(gen) {
            assert_eq!(*o, g);
        }
    }
}
