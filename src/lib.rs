#[macro_use]
extern crate lazy_static;

use std::collections::{HashMap, HashSet};

pub const BLOCK_SIZE: usize = 16; // 128 bit or 16 byte

pub mod encoding;
pub mod crypto;
pub mod random;
pub mod breaking;

use crypto::EncryptionMode;

pub fn xor(a: &[u8], b: &[u8]) -> Vec<u8> {
    assert_eq!(a.len(), b.len());

    a
        .iter()
        .zip(b)
        .map(|(a, b)| a ^ b)
        .collect()
}

pub fn single_byte_xor_decode(message: &[u8], key: u8) -> Vec<u8> {
    message.iter().map(|byte| byte ^ key).collect()
}

pub fn get_char_frequency(message: &str) -> HashMap<char, f64> {
    let mut counts = HashMap::new();
    let mut total = 0.0;

    for letter in message.chars() {
        *counts.entry(letter).or_insert(0.0) += 1.0;
        total += 1.0;
    }

    for (_key, count) in counts.iter_mut() {
        *count /= total;
    }

    counts
}

/// Score a sample char frequency against the reference char frequency in
/// english.
pub fn get_score(reference: &HashMap<char, f64>, sample: &HashMap<char, f64>) -> f64 {
    let mut score = 0.0;

    for (letter, freq) in reference.iter() {
        let sample_freq = sample.get(letter).or(Some(&0.0)).unwrap();

        score += (freq - sample_freq).abs();
    }

    score
}

pub fn repeating_key_encrypt(message: &[u8], key: &[u8]) -> Vec<u8> {
    let mut result = Vec::with_capacity(message.len());
    let mut i = 0;

    for byte in message {
        result.push(byte ^ key[i]);
        i = (i + 1) % key.len();
    }

    result
}

pub fn hamming_distance(s1: &[u8], s2: &[u8]) -> usize {
    assert_eq!(s1.len(), s2.len());

    s1.iter().zip(s2.iter()).map(|(b1, b2)| {
        let mut x = b1 ^ b2;
        let mut count = 0;

        while x != 0 {
            if x%2 == 1 {
                count += 1;
            }

            x >>= 1;
        }

        count
    }).sum()
}

/// Divide a slice of data into chunks of the given size, then return a vector
/// where the first entry contains a vector with the first byte of every chunk
/// and so on.
pub fn transpose(data: &[u8], size: usize) -> Vec<Vec<u8>> {
    let mut output = Vec::with_capacity(size);

    for _i in 0..size {
        output.push(Vec::with_capacity(data.len() / size));
    }

    for (index, byte) in data.iter().enumerate() {
        output[index % size].push(*byte);
    }

    output
}

/// Adds padding to a byte string so it is an even multiple of the block size
///
/// https://en.wikipedia.org/wiki/Padding_(cryptography)#PKCS#5_and_PKCS#7
pub fn pad(data: &[u8], block_size: usize) -> Vec<u8> {
    let mut output = data.to_vec();
    let missing_bytes = block_size - data.len()%block_size;

    for _i in 0..missing_bytes {
        output.push(missing_bytes as u8);
    }

    output
}

/// Removes padding added by the pad function
pub fn unpad(data: &[u8]) -> Option<&[u8]> {
    data.last().map(|padding| {
        if *padding == 0 {
            None
        } else {
            data.len().checked_sub(*padding as usize).map(|padding_mark| {
                let padding_bytes = &data[padding_mark..];

                // ensure every byte of the padding is equal, otherwise it might not
                // actually be a padding
                if padding_bytes.iter().all(|b| b == padding) {
                    Some(&data[..padding_mark])
                } else {
                    None
                }
            })
        }
    }).flatten().flatten()
}

pub fn guess_encryption_mode(blob: &[u8], input: &[u8]) -> EncryptionMode {
    assert!(input.len() >= 48, "At least 48 bytes required for test input");
    let first = input[0];
    assert!(input.iter().all(|i| *i == first), "All bytes must be equal");

    let mut blocks = HashSet::new();

    for block in blob.chunks(BLOCK_SIZE) {
        if !blocks.insert(block) {
            return EncryptionMode::ECB;
        }
    }

    EncryptionMode::CBC
}

#[cfg(test)]
mod tests {
    use super::{
        encoding::{b64, hex},
        xor, repeating_key_encrypt, hamming_distance, transpose, pad, unpad,
        guess_encryption_mode,
        random::encryption_oracle,
    };

    #[test]
    fn test_base64encode() {
        assert_eq!(b64::encode(b"any carnal pleasure."), "YW55IGNhcm5hbCBwbGVhc3VyZS4=");
        assert_eq!(b64::encode(b"any carnal pleasure"), "YW55IGNhcm5hbCBwbGVhc3VyZQ==");
        assert_eq!(b64::encode(b"any carnal pleasur"), "YW55IGNhcm5hbCBwbGVhc3Vy");
        assert_eq!(b64::encode(b"any carnal pleasu"), "YW55IGNhcm5hbCBwbGVhc3U=");
        assert_eq!(b64::encode(b"any carnal pleas"), "YW55IGNhcm5hbCBwbGVhcw==");
    }

    #[test]
    fn test_base64decode() {
        assert_eq!(b64::decode("YW55IGNhcm5hbCBwbGVhc3VyZS4="), b"any carnal pleasure.");
        assert_eq!(b64::decode("YW55IGNhcm5hbCBwbGVhc3VyZQ=="), b"any carnal pleasure");
        assert_eq!(b64::decode("YW55IGNhcm5hbCBwbGVhc3Vy"), b"any carnal pleasur");
        assert_eq!(b64::decode("YW55IGNhcm5hbCBwbGVhc3U="), b"any carnal pleasu");
        assert_eq!(b64::decode("YW55IGNhcm5hbCBwbGVhcw=="), b"any carnal pleas");
    }

    #[test]
    fn test_hextobase64() {
        assert_eq!(
            b64::encode(&hex::decode("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d")),
            "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
        );
    }

    #[test]
    fn test_fixed_xor() {
        let a = "1c0111001f010100061a024b53535009181c";
        let b = "686974207468652062756c6c277320657965";

        assert_eq!(hex::encode(&xor(&hex::decode(a), &hex::decode(b))), "746865206b696420646f6e277420706c6179");
    }

    #[test]
    fn test_repeating_key_xor() {
        let msg = "Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal";
        let key = b"ICE";
        let encrypted_string = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f";

        let encrypted = repeating_key_encrypt(&msg.bytes().collect::<Vec<u8>>(), key);
        let double_encrypted = repeating_key_encrypt(&encrypted, key);

        assert_eq!(hex::encode(&encrypted), encrypted_string);
        assert_eq!(String::from_utf8_lossy(&double_encrypted), msg);
    }

    #[test]
    fn test_hamming_distance() {
        let s1 = b"this is a test";
        let s2 = b"wokka wokka!!!";

        assert_eq!(hamming_distance(s1, s2), 37);
    }

    #[test]
    fn test_transpose() {
        let data = [1, 2, 1, 2, 1, 2];
        let output = transpose(&data[..], 2);

        assert_eq!(output, vec![vec![1, 1, 1], vec![2, 2, 2]]);
    }

    #[test]
    fn test_pad() {
        assert_eq!(pad(b"YELLOW SUBMARINE", 20), b"YELLOW SUBMARINE\x04\x04\x04\x04");
        assert_eq!(pad(b"YELLOW SUBMARINE", 16), b"YELLOW SUBMARINE\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10");
    }

    #[test]
    fn test_unpad() {
        assert_eq!(unpad(b"YELLOW SUBMARINE\x04\x04\x04\x04").unwrap(), b"YELLOW SUBMARINE");
        assert_eq!(unpad(b"YELLOW SUBMARINE\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10").unwrap(), b"YELLOW SUBMARINE");
        assert_eq!(unpad(b"ICE ICE BABY\x04\x04\x04\x04").unwrap(), b"ICE ICE BABY");

        assert!(unpad(b"ICE ICE BABY\x05\x05\x05\x05").is_none());
        assert!(unpad(b"ICE ICE BABY\x01\x02\x03\x04").is_none());
        assert!(unpad(b"\x00").is_none());
    }

    #[test]
    fn guess_encryption_oracle() {
        let input = vec![0; 48];

        for _ in 0..100 {
            let (mode, blob) = encryption_oracle(&input);
            assert_eq!(guess_encryption_mode(&blob, &input), mode);
        }
    }
}
