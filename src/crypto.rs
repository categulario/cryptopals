pub mod rijndael;

#[derive(Debug, PartialEq)]
pub enum EncryptionMode {
    ECB,
    CBC,
}
