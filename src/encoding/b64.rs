const B64_CHARS: [char; 64] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'];

fn byteb64val(c: char) -> u8 {
    match c {
        'A' => 0,
        'B' => 1,
        'C' => 2,
        'D' => 3,
        'E' => 4,
        'F' => 5,
        'G' => 6,
        'H' => 7,
        'I' => 8,
        'J' => 9,
        'K' => 10,
        'L' => 11,
        'M' => 12,
        'N' => 13,
        'O' => 14,
        'P' => 15,

        'Q' => 16,
        'R' => 17,
        'S' => 18,
        'T' => 19,
        'U' => 20,
        'V' => 21,
        'W' => 22,
        'X' => 23,
        'Y' => 24,
        'Z' => 25,
        'a' => 26,
        'b' => 27,
        'c' => 28,
        'd' => 29,
        'e' => 30,
        'f' => 31,

        'g' => 32,
        'h' => 33,
        'i' => 34,
        'j' => 35,
        'k' => 36,
        'l' => 37,
        'm' => 38,
        'n' => 39,
        'o' => 40,
        'p' => 41,
        'q' => 42,
        'r' => 43,
        's' => 44,
        't' => 45,
        'u' => 46,
        'v' => 47,

        'w' => 48,
        'x' => 49,
        'y' => 50,
        'z' => 51,
        '0' => 52,
        '1' => 53,
        '2' => 54,
        '3' => 55,
        '4' => 56,
        '5' => 57,
        '6' => 58,
        '7' => 59,
        '8' => 60,
        '9' => 61,
        '+' => 62,
        '/' => 63,
        _ => unreachable!(),
    }
}

pub fn encode(buffer: &[u8]) -> String {
    let mut ans = String::with_capacity(buffer.len()/3 * 4);
    let mut case = 0;
    let mut pieces: u8 = 0;

    for byte in buffer {
        match case {
            0 => {
                ans.push(B64_CHARS[(byte >> 2) as usize]);
                pieces = (byte % 4) << 4;
            },
            1 => {
                pieces += byte >> 4;
                ans.push(B64_CHARS[pieces as usize]);
                pieces = (byte % 16) << 2;
            },
            2 => {
                pieces += byte >> 6;
                ans.push(B64_CHARS[pieces as usize]);
                ans.push(B64_CHARS[(byte % 64) as usize]);
            },
            _ => unreachable!(),
        }

        case += 1;
        case = case%3;
    }

    // padding
    if case == 1 {
        ans.push(B64_CHARS[pieces as usize]);
        ans.push('=');
        ans.push('=');
    } else if case == 2 {
        ans.push(B64_CHARS[pieces as usize]);
        ans.push('=');
    }

    ans
}

pub fn decode(msg: &str) -> Vec<u8> {
    let mut output = Vec::with_capacity(msg.len() / 4 * 3);
    let mut curbyte: u8 = 0;
    let mut case = 0;

    for letter in msg.chars() {
        if letter == '=' {
            case = (case + 1) % 4;
            continue;
        }

        let value = byteb64val(letter);

        match case {
            0 => {
                curbyte = value << 2;
            },
            1 => {
                curbyte += value >> 4;
                output.push(curbyte);
                curbyte = value << 4;
            },
            2 => {
                curbyte += value >> 2;
                output.push(curbyte);
                curbyte = value << 6;
            },
            3 => {
                curbyte += value;
                output.push(curbyte);
            },
            _ => unreachable!(),
        }

        case = (case + 1) % 4;
    }

    if case == 1 || case == 2 || case == 3 {
        output.push(curbyte);
    }

    output
}
