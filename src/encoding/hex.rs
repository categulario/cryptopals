const HEX_CHARS: [char; 16] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];

// give the numeric value of a letter in hexadecimal
fn bytehexval(c: char) -> u8 {
    match c {
        '0' => 0,
        '1' => 1,
        '2' => 2,
        '3' => 3,
        '4' => 4,
        '5' => 5,
        '6' => 6,
        '7' => 7,
        '8' => 8,
        '9' => 9,
        'a' => 10,
        'b' => 11,
        'c' => 12,
        'd' => 13,
        'e' => 14,
        'f' => 15,
        _ => unreachable!(),
    }
}

pub fn decode(s: &str) -> Vec<u8> {
    let mut output = Vec::with_capacity(s.len()/2);
    let mut end = false;
    let mut cur:u8 = 0;

    for c in s.chars() {
        if end {
            cur += bytehexval(c);

            output.push(cur);
        } else {
            cur = bytehexval(c) << 4;
        }

        end = !end;
    }

    output
}

pub fn encode(buffer: &[u8]) -> String {
    let mut ans = String::with_capacity(buffer.len() * 2);

    for byte in buffer {
        let a = byte >> 4;
        let b = byte % 16;

        ans.push(HEX_CHARS[a as usize]);
        ans.push(HEX_CHARS[b as usize]);
    }

    ans
}
