use std::collections::HashMap;

pub fn decode(data: &str) -> HashMap<String, String> {
    data.split('&').map(|piece| {
        let pieces: Vec<_> = piece.split('=').collect();

        pieces.get(0).map(|key| {
            pieces.get(1).map(|value| {
                (key.to_string(), value.to_string())
            })
        })
    }).filter_map(Option::flatten).collect()
}

pub fn encode(data: &HashMap<String, String>) -> String {
    data.iter().map(|(key, value)| format!("{}={}", key, value)).collect::<Vec<_>>().join("&")
}

#[cfg(test)]
mod tests {
    use super::{encode, decode};

    #[test]
    fn test_url_decode() {
        assert_eq!(decode("a=b&foo=var"), vec![
            ("a".into(), "b".into()),
            ("foo".into(), "var".into()),
        ].into_iter().collect());
    }

    #[test]
    fn test_url_encode() {
        let encoded = encode(&vec![
            ("a".into(), "b".into()),
            ("c".into(), "d".into()),
        ].into_iter().collect());

        assert!(encoded.contains("a=b"));
        assert!(encoded.contains("c=d"));
        assert!(encoded.contains("&"));
    }
}
