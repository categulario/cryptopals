mod scheduler;
mod sbox;

const ROUNDS: usize = 10;

use crate::BLOCK_SIZE;

use scheduler::Scheduler;
use sbox::{sbox, inv_sbox};

struct Blocks<'a> {
    pos: usize,
    data: &'a [u8],
}

impl<'a> From<&'a [u8]> for Blocks<'a> {
    fn from(data: &[u8]) -> Blocks {
        Blocks {
            pos: 0,
            data,
        }
    }
}

impl<'a> Iterator for Blocks<'a> {
    type Item = &'a [u8];

    fn next(&mut self) -> Option<&'a [u8]> {
        if self.pos + BLOCK_SIZE <= self.data.len() {
            let ans = Some(&self.data[self.pos..(self.pos + BLOCK_SIZE)]);
            self.pos += BLOCK_SIZE;
            ans
        } else if self.pos < self.data.len() {
            let ans = Some(&self.data[self.pos..]);
            self.pos += BLOCK_SIZE;
            ans
        } else {
            None
        }
    }
}

fn to_a(src: &[u8]) -> [u8; 4] {
    let mut output = [0; 4];

    output.copy_from_slice(src);

    output
}

fn add_round_key(block: &mut [u8], round_key: [u8; 16]) {
    for (i, k) in round_key.iter().enumerate() {
        block[i] ^= k;
    }
}

fn sub_bytes(block: &mut [u8]) {
    for i in 0..block.len() {
        block[i] = sbox(block[i]);
    }
}

fn inv_sub_bytes(block: &mut [u8]) {
    for i in 0..block.len() {
        block[i] = inv_sbox(block[i]);
    }
}

fn shift_rows(block: &mut [u8]) {
    // second row shift one to the left
    let mut aux = block[1];
    block[1] = block[5];
    block[5] = block[9];
    block[9] = block[13];
    block[13] = aux;

    // third row two blocks to the left
    block.swap(2, 10);
    block.swap(6, 14);

    // fourth row shift three to the left or one to the right
    aux = block[15];
    block[15] = block[11];
    block[11] = block[7];
    block[7] = block[3];
    block[3] = aux;
}

fn inv_shift_rows(block: &mut [u8]) {
    // second row shift one to the right
    let mut aux = block[13];
    block[13] = block[9];
    block[9] = block[5];
    block[5] = block[1];
    block[1] = aux;

    //third row two blocks to the right
    block.swap(2, 10);
    block.swap(6, 14);

    // foruth row shift three to the right or one to the left
    aux = block[3];
    block[3] = block[7];
    block[7] = block[11];
    block[11] = block[15];
    block[15] = aux;
}

fn mix_column(col: [u8; 4]) -> [u8; 4] {
    let mut b = [0; 4];

    /* The array 'b' is each element of the array 'col' multiplied by 2
     * in Rijndael's Galois field
     * col[n] ^ b[n] is element n multiplied by 3 in Rijndael's Galois field */

    for c in 0..4 {
        /* h is 0xff if the high bit of col[c] is set, 0 otherwise */
        let h = ((col[c] as i8) >> 7) as u8; /* arithmetic right shift, thus shifting in either zeros or ones */
        b[c] = col[c] << 1; /* implicitly removes high bit because b[c] is an 8-bit char, so we xor by 0x1b and not 0x11b in the next line */
        b[c] ^= 0x1B & h; /* Rijndael's Galois field */
    }

    [
        b[0] ^ col[3] ^ col[2] ^ b[1] ^ col[1], /* 2 * a0 + a3 + a2 + 3 * a1 */
        b[1] ^ col[0] ^ col[3] ^ b[2] ^ col[2], /* 2 * a1 + a0 + a3 + 3 * a2 */
        b[2] ^ col[1] ^ col[0] ^ b[3] ^ col[3], /* 2 * a2 + a1 + a0 + 3 * a3 */
        b[3] ^ col[2] ^ col[1] ^ b[0] ^ col[0], /* 2 * a3 + a2 + a1 + 3 * a0 */
    ]
}

fn g_mul(mut a: u8, mut b: u8) -> u8 {
    let mut p: u8 = 0;

    for _ in 0..8 {
        if (b & 1) != 0 {
            p ^= a;
        }

        let hi_bit_set = (a & 0x80) != 0;
        a <<= 1;

        if hi_bit_set {
            a ^= 0x1B; /* x^8 + x^4 + x^3 + x + 1 */
        }

        b >>= 1;
    }

    p
}

fn inv_mix_column(col: [u8; 4]) -> [u8; 4] {
    [
        g_mul(14, col[0]) ^ g_mul(11, col[1]) ^ g_mul(13, col[2]) ^ g_mul(9, col[3]),
        g_mul(9, col[0]) ^ g_mul(14, col[1]) ^ g_mul(11, col[2]) ^ g_mul(13, col[3]),
        g_mul(13, col[0]) ^ g_mul(9, col[1]) ^ g_mul(14, col[2]) ^ g_mul(11, col[3]),
        g_mul(11, col[0]) ^ g_mul(13, col[1]) ^ g_mul(9, col[2]) ^ g_mul(14, col[3]),
    ]
}

fn mix_columns(block: &mut [u8]) {
    for i in 0..4 {
        let col = &mut block[i*4..(i+1)*4];
        col.copy_from_slice(&mix_column(to_a(col)));
    }
}

fn inv_mix_columns(block: &mut [u8]) {
    for i in 0..4 {
        let col = &mut block[i*4..(i+1)*4];
        col.copy_from_slice(&inv_mix_column(to_a(col)));
    }
}

/// This is where the data gets scrambled in rijndael.
///
/// Takes as input some data and the encryption key, returns rubbish.
fn encrypt_block(data: &[u8], key: &[u8]) -> Vec<u8> {
    assert_eq!(data.len(), BLOCK_SIZE);
    assert_eq!(key.len(), BLOCK_SIZE);

    let mut output: Vec<u8> = data.into();
    let mut scheduler = Scheduler::from(key);

    add_round_key(&mut output, scheduler.next().unwrap());

    for (i, round_key) in (0..ROUNDS).zip(scheduler) {
        sub_bytes(&mut output);
        shift_rows(&mut output);

        if i < ROUNDS - 1 {
            mix_columns(&mut output);
        }

        add_round_key(&mut output, round_key);
    }

    output
}

fn decrypt_block(data: &[u8], key: &[u8]) -> Vec<u8> {
    assert_eq!(data.len(), BLOCK_SIZE);
    assert_eq!(key.len(), BLOCK_SIZE);

    let mut output: Vec<u8> = data.into();
    let scheduler = Scheduler::from(key);
    let round_keys: Vec<_> = scheduler.take(ROUNDS + 1).collect();
    let mut scheduler = round_keys.into_iter().rev();

    for (i, round_key) in (0..ROUNDS).zip(&mut scheduler) {
        add_round_key(&mut output, round_key);

        if i > 0 {
            inv_mix_columns(&mut output);
        }

        inv_shift_rows(&mut output);
        inv_sub_bytes(&mut output);
    }

    add_round_key(&mut output, scheduler.next().unwrap());

    output
}

pub mod ecb {
    use super::{BLOCK_SIZE, encrypt_block, decrypt_block};

    pub fn encrypt(data: &[u8], key: &[u8]) -> Vec<u8> {
        assert_eq!(key.len(), BLOCK_SIZE);
        assert_eq!(data.len() % BLOCK_SIZE, 0);

        data.chunks(BLOCK_SIZE).map(|b| encrypt_block(b, key)).flatten().collect()
    }

    pub fn decrypt(data: &[u8], key: &[u8]) -> Vec<u8> {
        assert_eq!(key.len(), BLOCK_SIZE);
        assert_eq!(data.len() % BLOCK_SIZE, 0);

        data.chunks(BLOCK_SIZE).map(|b| decrypt_block(b, key)).flatten().collect()
    }
}

pub mod cbc {
    use super::{BLOCK_SIZE, encrypt_block, decrypt_block, super::super::xor};

    pub fn encrypt(data: &[u8], key: &[u8], iv: &[u8]) -> Vec<u8> {
        assert_eq!(key.len(), BLOCK_SIZE);
        assert_eq!(data.len() % BLOCK_SIZE, 0, "length of data to be encrypted is not a multiple of BLOCK_SIZE");

        data.chunks(BLOCK_SIZE).enumerate().fold(Vec::new(), |mut cipher, (i, chunk)| {
            let next_block = encrypt_block(&xor(chunk, if i == 0 {
                iv
            } else {
                &cipher[(cipher.len()-BLOCK_SIZE)..cipher.len()]
            }), key);

            cipher.extend(next_block);

            cipher
        })
    }

    pub fn decrypt(data: &[u8], key: &[u8], iv: &[u8]) -> Vec<u8> {
        assert_eq!(key.len(), BLOCK_SIZE);
        assert_eq!(data.len() % BLOCK_SIZE, 0);

        data.chunks(BLOCK_SIZE).enumerate().map(|(i, chunk)| {
            xor(&decrypt_block(chunk, key), if i == 0 {
                iv
            } else {
                &data[(i-1)*BLOCK_SIZE .. i*BLOCK_SIZE]
            })
        }).flatten().collect()
    }
}

pub mod ctr {
    use crate::{xor, BLOCK_SIZE};

    /// Performs CTR mode using 64 bit unsigned little endian nonce, 64 bit
    /// little endian block count (byte count / 16).
    ///
    /// might return less than BLOCK_SIZE bytes if the input data is less than
    /// that length.
    fn encrypt_block(data: &[u8], key: &[u8], nonce: u64, counter: u64) -> Vec<u8> {
        let mut input: Vec<u8> = nonce.to_le_bytes().to_vec();
        input.extend_from_slice(&counter.to_le_bytes());

        let output = super::encrypt_block(&input, key);

        xor(&output[..data.len()], data)
    }

    pub fn encrypt(data: &[u8], key: &[u8], nonce: u64) -> Vec<u8> {
        data.chunks(BLOCK_SIZE).enumerate().map(|(i, block)| {
            encrypt_block(&block, key, nonce, i as u64)
        }).collect::<Vec<_>>().concat()
    }

    pub fn decrypt(data: &[u8], key: &[u8], nonce: u64) -> Vec<u8> {
        data.chunks(BLOCK_SIZE).enumerate().map(|(i, block)| {
            encrypt_block(block, key, nonce, i as u64)
        }).collect::<Vec<_>>().concat()
    }

    #[cfg(test)]
    mod tests {
        use super::{encrypt, decrypt};
        use crate::{random::random_key, encoding::b64};
        use rand::prelude::*;

        #[test]
        fn challenge_18() {
            let cipher = "L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==";
            let key = b"YELLOW SUBMARINE";
            let nonce = 0;
            let message = "Yo, VIP Let\'s kick it Ice, Ice, baby Ice, Ice, baby ";

            let cipher_blob = b64::decode(cipher);

            assert_eq!(
                String::from_utf8_lossy(&decrypt(&cipher_blob, key, nonce)),
                message
            );

            assert_eq!(cipher_blob.len(), message.len());
        }

        #[test]
        fn ctr_encrypt_decrypt() {
            let message = "A chuchita la bolsearon, le quitaron la cartera!";
            let key = random_key();
            let nonce: u64 = random();

            assert_eq!(decrypt(&encrypt(&message.as_bytes(), &key, nonce), &key, nonce), message.as_bytes());
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{
        ecb, mix_column, mix_columns, inv_shift_rows, inv_mix_columns,
        inv_mix_column,
    };
    use crate::encoding::b64;

    #[test]
    fn simple_encrypt() {
        assert_eq!(b64::encode(&ecb::encrypt(b"YELLOW SUBMARINE", b"YELLOW SUBMARINE")), "0apPZXiSZUL7tt2HbNIFCA==");
    }

    #[test]
    fn simple_decrypt() {
        assert_eq!(String::from_utf8_lossy(&ecb::decrypt(&b64::decode("0apPZXiSZUL7tt2HbNIFCA=="), b"YELLOW SUBMARINE")), "YELLOW SUBMARINE");
    }

    #[test]
    fn test_mix_column() {
        assert_eq!(mix_column([0xdb, 0x13, 0x53, 0x45]), [0x8e, 0x4d, 0xa1, 0xbc]);
        assert_eq!(mix_column([0xf2, 0x0a, 0x22, 0x5c]), [0x9f, 0xdc, 0x58, 0x9d]);
        assert_eq!(mix_column([0x01, 0x01, 0x01, 0x01]), [0x01, 0x01, 0x01, 0x01]);
        assert_eq!(mix_column([0xc6, 0xc6, 0xc6, 0xc6]), [0xc6, 0xc6, 0xc6, 0xc6]);
        assert_eq!(mix_column([0xd4, 0xd4, 0xd4, 0xd5]), [0xd5, 0xd5, 0xd7, 0xd6]);
        assert_eq!(mix_column([0x2d, 0x26, 0x31, 0x4c]), [0x4d, 0x7e, 0xbd, 0xf8]);
    }

    #[test]
    fn test_inv_mix_column() {
        assert_eq!(inv_mix_column([0x8e, 0x4d, 0xa1, 0xbc]), [0xdb, 0x13, 0x53, 0x45]);
        assert_eq!(inv_mix_column([0x9f, 0xdc, 0x58, 0x9d]), [0xf2, 0x0a, 0x22, 0x5c]);
        assert_eq!(inv_mix_column([0x01, 0x01, 0x01, 0x01]), [0x01, 0x01, 0x01, 0x01]);
        assert_eq!(inv_mix_column([0xc6, 0xc6, 0xc6, 0xc6]), [0xc6, 0xc6, 0xc6, 0xc6]);
        assert_eq!(inv_mix_column([0xd5, 0xd5, 0xd7, 0xd6]), [0xd4, 0xd4, 0xd4, 0xd5]);
        assert_eq!(inv_mix_column([0x4d, 0x7e, 0xbd, 0xf8]), [0x2d, 0x26, 0x31, 0x4c]);
    }

    #[test]
    fn test_mix_columns() {
        let mut data = [0xdb, 0x13, 0x53, 0x45, 0xf2, 0x0a, 0x22, 0x5c, 0x01, 0x01, 0x01, 0x01, 0xc6, 0xc6, 0xc6, 0xc6];

        mix_columns(&mut data[..]);

        assert_eq!(data, [0x8e, 0x4d, 0xa1, 0xbc, 0x9f, 0xdc, 0x58, 0x9d, 0x01, 0x01, 0x01, 0x01, 0xc6, 0xc6, 0xc6, 0xc6]);
    }

    #[test]
    fn test_inv_shift_rows() {
        let mut block = [0x7a, 0xd5, 0xfd, 0xa7, 0x89, 0xef, 0x4e, 0x27, 0x2b, 0xca, 0x10, 0x0b, 0x3d, 0x9f, 0xf5, 0x9f];

        inv_shift_rows(&mut block);
        assert_eq!(block, [0x7a, 0x9f, 0x10, 0x27, 0x89, 0xd5, 0xf5, 0x0b, 0x2b, 0xef, 0xfd, 0x9f, 0x3d, 0xca, 0x4e, 0xa7]);
    }

    #[test]
    fn test_inv_mix_columns() {
        let mut data = [0x8e, 0x4d, 0xa1, 0xbc, 0x9f, 0xdc, 0x58, 0x9d, 0x01, 0x01, 0x01, 0x01, 0xc6, 0xc6, 0xc6, 0xc6];

        inv_mix_columns(&mut data);

        assert_eq!(data, [0xdb, 0x13, 0x53, 0x45, 0xf2, 0x0a, 0x22, 0x5c, 0x01, 0x01, 0x01, 0x01, 0xc6, 0xc6, 0xc6, 0xc6]);
    }
}
