use super::{BLOCK_SIZE, sbox::sub, to_a};

const KEY_LENGTH: usize = 4;

#[derive(Default)]
pub struct Scheduler {
    round: usize,
    round_constant: [u8; 4],
    state: [[u8; 4]; 4],
}

impl Scheduler {
    pub fn round_constant(&self) -> [u8; 4] {
        self.round_constant
    }
}

impl From<&[u8]> for Scheduler {
    fn from(key: &[u8]) -> Scheduler {
        assert_eq!(key.len(), BLOCK_SIZE);

        Scheduler {
            round: 0,
            state: [to_a(&key[0..4]), to_a(&key[4..8]), to_a(&key[8..12]), to_a(&key[12..])],
            ..Default::default()
        }
    }
}

impl From<&[u8; 16]> for Scheduler {
    fn from(key: &[u8; 16]) -> Scheduler {
        Scheduler {
            round: 0,
            state: [to_a(&key[0..4]), to_a(&key[4..8]), to_a(&key[8..12]), to_a(&key[12..])],
            ..Default::default()
        }
    }
}

impl Iterator for Scheduler {
    type Item = [u8; 16];

    fn next(&mut self) -> Option<Self::Item> {
        let mut ans = [0; 16];

        &mut ans[0..4].copy_from_slice(&self.state[0]);
        &mut ans[4..8].copy_from_slice(&self.state[1]);
        &mut ans[8..12].copy_from_slice(&self.state[2]);
        &mut ans[12..].copy_from_slice(&self.state[3]);

        // update state
        self.round += 1;

        self.round_constant = match self.round {
            0 => unreachable!(),
            1 => [0x01, 0x00, 0x00, 0x00],
            _i if self.round_constant[0] < 0x80 => [2*self.round_constant[0], 0x00, 0x00, 0x00],
            _i => [(2u8.overflowing_mul(self.round_constant[0]).0)^0x1B, 0x00, 0x00, 0x00],
        };

        let mut next_state = [[0; 4]; KEY_LENGTH];

        for i in self.round*4..(self.round+1)*4 {
            next_state[i%4] = if i % KEY_LENGTH == 0 {
                    xor(xor(self.state[i%4], sub(rot(self.state[3]))), self.round_constant())
                } else {
                    xor(self.state[i%4], next_state[(i-1)%4])
                };
        }

        self.state = next_state;

        Some(ans)
    }
}

fn rot(a: [u8; 4]) -> [u8; 4] {
    [a[1], a[2], a[3], a[0]]
}

fn xor(a: [u8; 4], b: [u8; 4]) -> [u8; 4] {
    [a[0]^b[0], a[1]^b[1], a[2]^b[2], a[3]^b[3]]
}

#[cfg(test)]
mod tests {
    use super::Scheduler;

    #[test]
    fn round_constants() {
        let mut s = Scheduler::from(b"YELLOW SUBMARINE");

        assert_eq!(s.round_constant(), [0x00, 0x00, 0x00, 0x00]);

        s.next().unwrap();
        assert_eq!(s.round_constant(), [0x01, 0x00, 0x00, 0x00]);
        s.next().unwrap();
        assert_eq!(s.round_constant(), [0x02, 0x00, 0x00, 0x00]);
        s.next().unwrap();
        assert_eq!(s.round_constant(), [0x04, 0x00, 0x00, 0x00]);
        s.next().unwrap();
        assert_eq!(s.round_constant(), [0x08, 0x00, 0x00, 0x00]);
        s.next().unwrap();
        assert_eq!(s.round_constant(), [0x10, 0x00, 0x00, 0x00]);
        s.next().unwrap();
        assert_eq!(s.round_constant(), [0x20, 0x00, 0x00, 0x00]);
        s.next().unwrap();
        assert_eq!(s.round_constant(), [0x40, 0x00, 0x00, 0x00]);
        s.next().unwrap();
        assert_eq!(s.round_constant(), [0x80, 0x00, 0x00, 0x00]);
        s.next().unwrap();
        assert_eq!(s.round_constant(), [0x1B, 0x00, 0x00, 0x00]);
        s.next().unwrap();
        assert_eq!(s.round_constant(), [0x36, 0x00, 0x00, 0x00]);
    }

    #[test]
    fn simple_schedule() {
        let orig_key = b"YELLOW SUBMARINE";
        let mut arr = [0_u8; 16];
        arr.copy_from_slice(orig_key);
        let mut s = Scheduler::from(orig_key);

        assert_eq!(s.next(), Some(arr));
        assert_eq!(s.next(), Some([99, 106, 34, 76, 44, 61, 2, 31, 121, 127, 79, 94, 43, 54, 1, 27]));
    }
}
