use super::utilities::guess_block_size;

use crate::BLOCK_SIZE;

/// Guesses the prefix size for a CBC oracle.
///
/// For CBC it is important to realize that the blocks of data start to change
/// starting with the first block where input appears, but previous blocks
/// remain unchanged so using this fact we can find the first block that
/// changes when we send empty input and 1-byte input to the oracle.
///
/// ## Panics
///
/// if the key used in the oracle changes or if the encryption algorithm is not
/// CBC
fn guess_prefix_size<F>(f: F, block_size: usize) -> usize
where
    F: Fn(&[u8]) -> Vec<u8>,
{
    // the ciphertext with an empty input
    let cipher_0 = f(b"");

    // the ciphertext with an input of length 1
    let cipher_1 = f(b"Z");

    // the index of the first item that differs between cipher_0 and cipher_1
    // is the first position affected by the input and will be stored in
    // `differ_pos`. The first block that differs is stored in `differ_block`
    // and belongs to `cipher_1`
    let (differ_pos, (_, differ_block)) = cipher_0
        .chunks(block_size)
        .zip(cipher_1.chunks(block_size))
        .enumerate()
        .filter(|(_i, (c0, c1))| c0 != c1)
        .next()
        .unwrap();

    // from the next code the important thing is that we're computing the value
    // of input_size_that_fits_block_boundary variable, that is exactly how
    // much input you can give to the oracle to make it touch (but not
    // trespass) the block boundary.
    let mut differ_block = differ_block.to_vec();
    let mut input_size_that_fits_block_boundary = 1;

    for i in 2..(block_size * 2) {
        let cipher = f(&vec![b'Z'; i]);
        let block_of_interest = &cipher[differ_pos*block_size..(differ_pos+1)*block_size];

        if block_of_interest == differ_block {
            input_size_that_fits_block_boundary = i - 1;
            break;
        } else {
            differ_block = block_of_interest.to_vec();
        }
    }

    block_size * differ_pos + (block_size - input_size_that_fits_block_boundary)
}

/// Using the properties of CBC we'll craft an input and manipulate the
/// ciphertext to produce an admin=true output.
///
/// https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Cipher_block_chaining_(CBC)
///
/// ## Panics
///
/// if f is not ECB
pub fn bitflipping_attack<F>(oracle: F) -> Vec<u8>
where
    F: Fn(&[u8]) -> Vec<u8>,
{
    // The plan is to take over two consecutive blocks, so first lets get the
    // size of the prefix
    let block_size = guess_block_size(&oracle);
    let prefix_size = guess_prefix_size(&oracle, block_size);

    // of the two crafted blocks, the first one doesn't really matter, its
    // decrypted plaintext will be rubish, but the second one will hopefully
    // decrypt to contain admin=true

    // First add some padding
    let leftpadding = (block_size - prefix_size%block_size)%block_size;
    let mut input = vec![b'A'; leftpadding];

    // next add a block of rubish
    input.extend_from_slice(&vec![b'A'; block_size]);

    // and the block that will make the trick
    let target = b"_admin_true";
    input.extend_from_slice(&vec![b'A'; block_size - target.len()]);
    input.extend_from_slice(target);

    let mut ciphertext = oracle(&input);

    // now we'll edit the ciphertext of the first crafted block exactly in the
    // relative positions of the _ in the second one, to transform the frist
    // in ; and se second in =

    // How many blocks are there previous to the one I'm editting
    let prev_blocks = (prefix_size + leftpadding) / block_size;

    let semicolon_pos = prev_blocks * block_size + (block_size - target.len());
    let equal_pos = prev_blocks * block_size + (block_size - target.len() + target.iter().rposition(|c| *c == b'_').unwrap());

    ciphertext[semicolon_pos] ^= b';' ^ b'_';
    ciphertext[equal_pos] ^= b'=' ^ b'_';

    ciphertext
}

fn padding_oracle_single_byte<F>(oracle: F, c1: &[u8], c2: &[u8], byte_pos: usize, result: &[u8], skip: bool) -> Option<u8>
where
    F: Fn(&[u8]) -> bool,
{
    let mut tampered = c1.to_vec();

    tampered.extend_from_slice(c2);

    let wanted_padding = (BLOCK_SIZE - byte_pos) as u8;  // 1, 2, 3, ...

    // set all bytes after `byte_pos` to something that will XOR to the
    // expected padding
    for i in (byte_pos+1)..BLOCK_SIZE {
        let b = tampered.get_mut(i).unwrap();
        *b = result[i] ^ c1[i] ^ wanted_padding;
    }

    let mut skept = false;

    // try every possible byte at `byte_pos` for correct padding
    for b in 0..=255u8 {
        let c1_last = tampered.get_mut(byte_pos).unwrap();

        *c1_last = b;

        if oracle(&tampered) {
            if skip && !skept {
                skept = true;
                continue;
            }

            let found_byte = b ^ wanted_padding ^ c1[byte_pos];

            return Some(found_byte)
        }
    }

    None
}

/// Decrypt a single block of data given two contiguous blocks of information.
///
/// The second block is to be decrypted and the first one is used to xor against
/// found bytes of data to obtain the plaintext.
fn padding_oracle_single_block<F>(oracle: F, c1: &[u8], c2: &[u8]) -> Vec<u8>
where
    F: Fn(&[u8]) -> bool,
{
    let mut result = vec![b'A'; c1.len()];

    result[BLOCK_SIZE - 1] = padding_oracle_single_byte(&oracle, c1, c2, BLOCK_SIZE - 1, &result, false).unwrap();

    if let Some(found_byte) = padding_oracle_single_byte(&oracle, c1, c2, BLOCK_SIZE - 2, &result, false) {
        result[BLOCK_SIZE - 2] = found_byte;
    } else {
        result[BLOCK_SIZE - 1] = padding_oracle_single_byte(&oracle, c1, c2, BLOCK_SIZE - 1, &result, true).unwrap();
        result[BLOCK_SIZE - 2] = padding_oracle_single_byte(&oracle, c1, c2, BLOCK_SIZE - 2, &result, false).unwrap();
    }

    for byte_pos in (0..BLOCK_SIZE-2).rev() {
        result[byte_pos] = padding_oracle_single_byte(&oracle, c1, c2, byte_pos, &result, false).unwrap_or(b'x');
    }

    result
}

/// Perform the padding oracle attack using given oracle, ciphertext and
/// initialization vector.
pub fn padding_oracle_attack<F>(oracle: F, ciphertext: &[u8], iv: &[u8]) -> Vec<u8>
where
    F: Fn(&[u8]) -> bool,
{
    let num_blocks = ciphertext.len() / BLOCK_SIZE;
    let mut res = Vec::with_capacity(ciphertext.len());

    let c1 = iv;
    let c2 = &ciphertext[..BLOCK_SIZE];

    res.extend_from_slice(&padding_oracle_single_block(&oracle, c1, c2));

    // Just iterate the blocks and use padding_oracle_single_block on each
    // independently
    for i in 0..num_blocks-1 {
        res.extend_from_slice(&padding_oracle_single_block(
            &oracle,
            &ciphertext[BLOCK_SIZE*i..BLOCK_SIZE*(i+1)],
            &ciphertext[BLOCK_SIZE*(i+1)..BLOCK_SIZE*(i+2)]
        ));
    }

    res
}

#[cfg(test)]
mod tests {
    use super::{
        guess_prefix_size, bitflipping_attack, padding_oracle_attack,
        padding_oracle_single_block,
    };

    use crate::random::random_key;
    use crate::{pad, unpad, BLOCK_SIZE};
    use crate::crypto::rijndael::cbc;
    use crate::encoding::b64;

    mod oracle {
        use crate::breaking::utilities::quote;
        use crate::crypto::rijndael::cbc;
        use crate::{pad, unpad, BLOCK_SIZE};

        ///  The first function should take an arbitrary input string, prepend the
        ///  string:
        ///
        /// "comment1=cooking%20MCs;userdata="
        ///
        /// .. and append the string:
        ///
        /// ";comment2=%20like%20a%20pound%20of%20bacon"
        ///
        /// The function should quote out the ";" and "=" characters.
        ///
        /// The function should then pad out the input to the 16-byte AES block length
        /// and encrypt it under the random AES key.
        pub fn challenge_16_oracle(input: &str, key: &[u8], iv: &[u8]) -> Vec<u8> {
            let mut data = String::from("comment1=cooking%20MCs;userdata=");

            data.push_str(&quote(input));

            data.push_str(";comment2=%20like%20a%20pound%20of%20bacon");

            cbc::encrypt(&pad(data.as_bytes(), BLOCK_SIZE), key, iv)
        }

        pub fn padding(ciphertext: &[u8], key: &[u8], iv: &[u8]) -> bool {
            unpad(&cbc::decrypt(ciphertext, key, iv)).is_some()
        }
    }

    #[test]
    fn test_guess_prefix_size_cbc() {
        let key = random_key();
        let iv = random_key();

        assert_eq!(guess_prefix_size(|input| {
            let mut data = b"prefix".to_vec();

            data.extend_from_slice(input);

            cbc::encrypt(&pad(&data, BLOCK_SIZE), &key, &iv)
        }, BLOCK_SIZE), 6);

        assert_eq!(guess_prefix_size(|input| {
            let mut data = Vec::new();

            data.extend_from_slice(input);

            cbc::encrypt(&pad(&data, BLOCK_SIZE), &key, &iv)
        }, BLOCK_SIZE), 0);

        assert_eq!(guess_prefix_size(|input| {
            let mut data = vec![0; BLOCK_SIZE];

            data.extend_from_slice(input);

            cbc::encrypt(&pad(&data, BLOCK_SIZE), &key, &iv)
        }, BLOCK_SIZE), BLOCK_SIZE);

        assert_eq!(guess_prefix_size(|input| {
            let mut data = vec![0; BLOCK_SIZE + BLOCK_SIZE / 2];

            data.extend_from_slice(input);

            cbc::encrypt(&pad(&data, BLOCK_SIZE), &key, &iv)
        }, BLOCK_SIZE), BLOCK_SIZE + BLOCK_SIZE / 2);

        assert_eq!(guess_prefix_size(|input| {
            let mut data = b"comment1=cooking%20MCs;userdata=".to_vec();

            data.extend_from_slice(input);

            data.extend_from_slice(b";comment2=%20like%20a%20pound%20of%20bacon");

            cbc::encrypt(&pad(&data, BLOCK_SIZE), &key, &iv)
        }, BLOCK_SIZE), 32);
    }

    #[test]
    fn challenge_16() {
        let key = random_key();
        let iv = random_key();

        let ans = bitflipping_attack(|input| {
            oracle::challenge_16_oracle(&String::from_utf8_lossy(input), &key, &iv)
        });

        assert!(String::from_utf8_lossy(unpad(&cbc::decrypt(&ans, &key, &iv)).unwrap()).contains(";admin=true;"));
    }

    #[test]
    fn test_padding_oracle_single_block() {
        let key = random_key();
        let iv = random_key();
        let msg = "abcdefghijklmnop";
        let ciphertext = cbc::encrypt(&pad(msg.as_bytes(), BLOCK_SIZE), &key, &iv);

        let decrypted = padding_oracle_single_block(|input| {
            oracle::padding(input, &key, &iv)
        }, &iv, &ciphertext[..BLOCK_SIZE]);

        assert_eq!(String::from_utf8_lossy(&decrypted), msg);
    }

    #[test]
    fn test_padding_oracle_single_block_custom_iv() {
        let key = vec![189, 200, 232, 170, 255, 9, 132, 6, 215, 121, 236, 39, 96, 18, 25, 75];
        let iv = vec![ 27, 22, 54, 21, 178, 167, 193, 143, 161, 177, 227, 193, 125, 187, 132, 136];
        let msg = pad(&vec![
            48, 48, 48, 48, 48, 48, 78, 111, 119, 32, 116, 104, 97, 116, 32, 116,
            104, 101, 32, 112, 97, 114, 116, 121, 32, 105, 115, 32, 106, 117, 109, 112,
            105, 110, 103,
        ], BLOCK_SIZE);

        let ciphertext = cbc::encrypt(&msg, &key, &iv);

        let decrypted = padding_oracle_single_block(|input| {
            oracle::padding(input, &key, &iv)
        }, &ciphertext[BLOCK_SIZE..BLOCK_SIZE*2], &ciphertext[BLOCK_SIZE*2..BLOCK_SIZE*3]);

        assert_eq!(decrypted, &msg[BLOCK_SIZE*2..BLOCK_SIZE*3]);
    }

    #[test]
    fn test_padding_oracle_single_block_custom_iv_2() {
        let key = [ 109, 239, 211, 126, 47, 155, 182, 146, 94, 90, 95, 95, 226, 124, 4, 136, ];
        let iv = [ 222, 97, 61, 175, 107, 153, 200, 118, 200, 48, 198, 67, 243, 120, 4, 208, ];
        let msg = pad(&b64::decode("MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw=="), BLOCK_SIZE);
        let ciphertext = cbc::encrypt(&msg, &key, &iv);

        let decrypted = padding_oracle_single_block(|input| {
            oracle::padding(input, &key, &iv)
        }, &ciphertext[BLOCK_SIZE..BLOCK_SIZE*2], &ciphertext[BLOCK_SIZE*2..BLOCK_SIZE*3]);

        assert_eq!(decrypted, &msg[BLOCK_SIZE*2..BLOCK_SIZE*3]);
    }

    #[test]
    fn challenge_17() {
        let key = random_key();
        let iv = random_key();

        dbg!(&key);
        dbg!(&iv);

        let strings = [
            "MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=",
            "MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=",
            "MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==",
            "MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==",
            "MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl",
            "MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==",
            "MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==",
            "MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=",
            "MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=",
            "MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93",
        ];

        for string in strings.iter() {
            let bytes = b64::decode(string);
            dbg!(&bytes);
            println!("{}", String::from_utf8_lossy(&bytes));
            let ciphertext = cbc::encrypt(&pad(&bytes, BLOCK_SIZE), &key, &iv);
            let ans = padding_oracle_attack(|input| {
                oracle::padding(input, &key, &iv)
            }, &ciphertext, &iv);

            assert_eq!(bytes, unpad(&ans).unwrap());
        }
    }
}
