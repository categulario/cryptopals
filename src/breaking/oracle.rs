use crate::breaking::utilities::quote;
use crate::crypto::rijndael::cbc;
use crate::{pad, BLOCK_SIZE};

///  The first function should take an arbitrary input string, prepend the
///  string:
///
/// "comment1=cooking%20MCs;userdata="
///
/// .. and append the string:
///
/// ";comment2=%20like%20a%20pound%20of%20bacon"
///
/// The function should quote out the ";" and "=" characters.
///
/// The function should then pad out the input to the 16-byte AES block length
/// and encrypt it under the random AES key.
pub fn challenge_16_oracle(input: &str, key: &[u8], iv: &[u8]) -> Vec<u8> {
    let mut data = String::from("comment1=cooking%20MCs;userdata=");

    data.push_str(&quote(input));

    data.push_str(";comment2=%20like%20a%20pound%20of%20bacon");

    cbc::encrypt(&pad(data.as_bytes(), BLOCK_SIZE), key, iv)
}
