use std::collections::HashSet;

use crate::crypto::rijndael::ecb;
use crate::pad;
use crate::BLOCK_SIZE;

/// Quote out some troublesome characters
pub fn quote(email: &str) -> String {
    email
        .replace('=', "")
        .replace('&', "")
        .replace(';', "")
}

/// For challenge 13
/// https://cryptopals.com/sets/2/challenges/13
///
/// Gets the user profile given an email
pub fn get_profile(email: &str) -> String {
    format!("email={}&uid=10&role=user", quote(email))
}

/// Also for challenge 13
///
/// Gets an encrypted user profile given an email and encryption key
pub fn encrypted_profile(email: &[u8], key: &[u8]) -> Vec<u8> {
    let profile = get_profile(&String::from_utf8_lossy(email));
    ecb::encrypt(&pad(profile.as_bytes(), BLOCK_SIZE), key)
}

/// returns the index of the first repeated block, or None if they're all
/// different.
///
/// Mostly useful with ECB
pub fn get_repeated_block(data: &[u8], block_size: usize) -> Option<(&[u8], usize)> {
    let mut blocks = HashSet::new();

    for (i, currrent_block) in data.chunks(block_size).enumerate() {
        if blocks.contains(currrent_block) {
            return Some((currrent_block, i));
        } else {
            blocks.insert(currrent_block);
        }
    };

    None
}

/// the oracle function takes some input and returns encrypted output. By
/// changing the size of the input we can guess the size of the block.
pub fn guess_block_size<F>(oracle: F) -> usize
    where F: Fn(&[u8]) -> Vec<u8>
{
    let encrypted_data_size = oracle(b"").len();
    let mut i = 1;

    loop {
        let new_size = oracle(&vec![b'A'; i*2]).len();

        if new_size != encrypted_data_size {
            break new_size - encrypted_data_size;
        }

        i += 1;
    }
}

#[cfg(test)]
mod tests {
    use super::{get_profile, quote};

    #[test]
    fn test_profile() {
        let email = "foo@bar.com";

        assert!(get_profile(email).contains("uid=10"));
        assert!(get_profile(email).contains("role=user"));
        assert!(get_profile(email).contains(&format!("email={}", email)));
    }

    #[test]
    fn test_profile_hack() {
        let email = "foo@bar.com&role=admin";

        assert!(!get_profile(email).contains("role=admin"));
    }

    #[test]
    fn test_quote() {
        assert_eq!(quote("=;&"), "");
    }
}
