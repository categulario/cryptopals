use std::collections::HashMap;

use crate::{
    breaking::LETTER_FREQUENCY, hamming_distance, transpose,
    single_byte_xor_decode, get_char_frequency, get_score,
    repeating_key_encrypt,
};

/// tries to guess the best single-byte key available for a possibly encrypted text
/// returns they key and its error measure
pub fn guess_single_byte_xor_key(letter_frequency: &HashMap<char, f64>, message: &[u8]) -> (u8, f64) {
    let mut ratings = Vec::new();

    for possible_key in 0..255_u8 {
        let possible_message = single_byte_xor_decode(&message, possible_key);

        let char_frequency = get_char_frequency(&String::from_utf8_lossy(&possible_message));
        let score = get_score(&letter_frequency, &char_frequency);

        ratings.push((possible_key, score));
    }

    ratings.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());

    ratings[0]
}

/// given a size in bytes and an encrypted message, return the likelyhood of
/// this size being the size of the key in a repeating key xor encryption.
fn score_size(size: usize, msg: &[u8]) -> f64 {
    let mut first_start = 0;
    let mut first_end = size;
    let mut end = size * 2;
    let mut result = 0.0;
    let mut count = 0;

    while end < msg.len() {
        result += (hamming_distance(&msg[first_start..first_end], &msg[first_end..end]) as f64) / size as f64;
        count += 1;
        first_start += size;
        first_end += size;
        end += size;
    }

    result / count as f64
}

/// Break repeating-key XOR encription
pub fn repeating_key_xor(bytes: &[u8]) -> (Vec<u8>, Vec<u8>) {
    let mut size_scores: Vec<_> = (2..=40).map(|size| {
        (size, score_size(size, &bytes))
    }).collect();

    size_scores.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());

    let key_length = size_scores[0].0;

    let blocks = transpose(&bytes, key_length);

    let key: Vec<_> = blocks.into_iter().map(|block| {
        guess_single_byte_xor_key(&LETTER_FREQUENCY, &block).0
    }).collect();

    (key.clone(), repeating_key_encrypt(&bytes, &key))
}

#[cfg(test)]
mod tests {
    use crate::encoding::b64;

    use super::repeating_key_xor;

    #[test]
    fn challenge_6() {
        let data = include_str!("../../data/6.txt");
        let b64data: String = data.split('\n').collect::<Vec<_>>().join("");
        let bytes = b64::decode(&b64data);

        let (key, _message) = repeating_key_xor(&bytes);

        assert_eq!(key, b"Terminator X: Bring the noise");
    }
}
