use crate::pad;

use super::utilities::{get_repeated_block, guess_block_size};

/// # challenge 13
///
/// f is a function that takes an email and returns the encrypted profile for
/// it, such that f("foo@bar.com") == AES128ECB("foo@bar.com&uid=10&role=user")
pub fn make_role_admin<F>(oracle: F) -> Vec<u8>
    where F: Fn(&[u8]) -> Vec<u8>
{
    let block_size = guess_block_size(&oracle);

    // forge a block with email=----------admin----------- where - is a padding
    // byte, then take the second block of the output.
    let mut forged_input = vec![b'A'; block_size - b"email=".len()];
    forged_input.extend_from_slice(&pad(b"admin", block_size));

    let admin_cipher = oracle(&forged_input);
    let admin_block = &admin_cipher[block_size..2*block_size];

    // craft an email that puts "user" at the begining of last block.
    // Grow the email until next block is created, then make email size be 3
    // bytes longer
    let initial_size = oracle(b"").len();

    let mut i = 1;
    let email_size = loop {
        let new_size = oracle(&vec![b'A'; i]).len();

        if new_size != initial_size {
            break i + 4;
        }

        i += 1;
    };

    // email=aaaaaaaaaaaaa&uid=10&role=user
    // ----------------++++++++++++++++----------------

    let mut output = oracle(&vec![b'A'; email_size]);
    let output_len = output.len();
    let last_block = &mut output[output_len-block_size..];
    // replace last block with crafted block and previously obtained email
    last_block.copy_from_slice(admin_block);

    output
}

/// Guess the prefix size for an ECB oracle, if the function is not ECB returns
/// None.
///
/// let's guess the size of the padding by sending three blocks of identical
/// bytes and finding them in the output. There are two cases:
///
/// 1. There are three identical blocks. Let i be the 0-index of the first of
///    them, then there are i*BLOCK_SIZE bytes of padding.
/// 2. There are only two identical blocks. Here we have to decrease the size
///    of the input until there is only one (identical to the previous two)
///    block. At that point we know the size of the padding: let i be the
///    0-index of the remaining block and k the amount of bytes we had to
///    remove from the input to switch from two identical blocks to one.  Then
///    there are min(i-1, 0)*BLOCK_SIZE - (BLOCK_SIZE - k + 1) bytes of
///    padding.
pub fn guess_prefix_size<F>(f: F, block_size: usize) -> Option<usize>
where F: Fn(&[u8]) -> Vec<u8>
{
    let input = vec![0; block_size*3];
    let output = f(&input);

    let (block, i) = get_repeated_block(&output, block_size)?;

    if (i+2)*block_size <= output.len() && block == &output[(i+1)*block_size..(i+2)*block_size] {
        // Case 1: three identical blocks
        Some((i-1)*block_size)
    } else {
        // Case 2: only two blocks

        // let k be the minimum number of bytes we have to remove to get only
        // one known block
        let mut k = 1;
        let k = loop {
            let output = f(&vec![0; block_size*3 - k]);

            // let j be the index of the first known block
            let mut j = 0;
            let j = loop {
                if &output[j*block_size..(j+1)*block_size] == block {
                    break j;
                }

                j += 1;
            };

            if (j+2)*block_size > output.len() || &output[(j+1)*block_size..(j+2)*block_size] != block {
                break k;
            }

            k += 1;
        };

        Some(i.saturating_sub(2)*block_size + k - 1)
    }
}

/// return the size of the input that puts the given byte position as the
/// last byte of an attacker controlled block.
fn get_input_size(byte_pos: usize, prefix_size: usize, block_size: usize) -> usize {
    (block_size - (1 + byte_pos + prefix_size)%block_size)%block_size
}

/// Compute a test input that will allow playing with the last byte to retrieve
/// one byte from the encoded message.
fn make_test_block(byte_pos: usize, prefix_size: usize, block_size: usize, data: &[u8]) -> (Vec<u8>, usize) {
    let input_size = get_input_size(byte_pos, prefix_size, block_size);
    // the size of the space available in the last block that contains prefix,
    // this space will be filled with input.
    let gap_size = block_size - prefix_size%block_size;

    let mut input = if byte_pos < block_size {
        // input and decrypted data fit into the gap, create a vector with
        // input_size bytes
        let mut input = vec![b'A'; input_size];

        // add all the decrypted data so far
        input.extend_from_slice(&data);

        input
    } else {
        // just take the last block of decrypted data
        let mut input = vec![b'A'; gap_size];

        input.extend_from_slice(&data[data.len()-block_size+1..]);

        input
    };

    // add an extra byte for guessing
    input.push(0);

    let block_pos = (prefix_size + input.len() - 1) / block_size;

    // return it along with the index of this block in the output
    (input, block_pos)
}

/// assume you have an oracle that lets you input arbitrary bytes and return
/// the result of:
///
/// aes_128_ecb(sobre prefix || input || data)
///
/// then this function returns the decrypted data.
///
/// ## Panics
///
/// if f is not ECB
pub fn byte_at_a_time_ecb_with_prefix<F>(f: F) -> Vec<u8>
    where F: Fn(&[u8]) -> Vec<u8>
{
    // First let's guess the block size
    let block_size = guess_block_size(&f);

    // next guess how many bytes of prefix we have
    let prefix_size = guess_prefix_size(&f, block_size).unwrap();

    // and how many bytes of information we have
    let orig_num_blocks = f(b"").len() / block_size;
    let mut i = 1;
    let padding_size = loop {
        if f(&vec![0; i]).len()/block_size > orig_num_blocks {
            break i - 1;
        }
        i += 1;
    };

    // and this is how much data we can decrypt
    let data_size = orig_num_blocks*block_size - prefix_size - padding_size - 1;

    // this vector will contain all the decrypted data
    let mut data = Vec::with_capacity(data_size);

    // We start with a paddgin of block_size-1 bytes to obtain the first byte
    // of the encrypted data, then we move to obtain the next byte and so on
    // until decryption is finished.
    for byte_pos in 0..data_size {
        // the size of an input that puts the target byte at the end of a block
        let input_size = get_input_size(byte_pos, prefix_size, block_size);

        // the position of the target block in the output
        let block_num = (prefix_size + input_size + byte_pos) / block_size;

        // feed the oracle an input of a size that puts the target byte at the
        // end of the `block_num` block.
        let encrypted_data = f(&vec![b'A'; input_size]);

        // retrieve the target block. Test blocks will be compared against this
        let target_block = &encrypted_data[(block_num*block_size)..((block_num+1)*block_size)];

        // make a test block of an appropiate size, we will change its last byte
        // with a test byte until it matches the target block.
        let (mut test_block, test_block_num) = make_test_block(byte_pos, prefix_size, block_size, &data);

        let test_index = test_block.len()-1;

        // try all possible byte values
        for i in 0..=255_u8 {
            // set current test byte at the end of the test block
            test_block[test_index] = i;
            let test_encrypted = f(&test_block);

            // get the encrypted test block and compare agains target_block
            if &test_encrypted[test_block_num*block_size..(test_block_num+1)*block_size] == target_block {
                data.push(i);

                break;
            }
        }
    }

    data
}

#[cfg(test)]
mod tests {
    use rand::prelude::*;

    use super::{
        guess_block_size, byte_at_a_time_ecb_with_prefix, make_role_admin,
    };

    use crate::crypto::rijndael::ecb;
    use crate::{pad, unpad, BLOCK_SIZE};
    use crate::random::{random_key, random_bytes, random_string, random_count_bytes};
    use crate::breaking::ecb::guess_prefix_size;
    use crate::crypto::rijndael::cbc;
    use crate::encoding::{b64, url};
    use crate::breaking::utilities::encrypted_profile;

    #[test]
    fn test_guess_block_size() {
        let key = b"yellow submarine";

        assert_eq!(guess_block_size(|input| {
            ecb::encrypt(&pad(input, BLOCK_SIZE), key)
        }), BLOCK_SIZE);
    }

    #[test]
    fn test_guess_padding_size_matching_block_size() {
        let padding_size = 16;
        let key = random_key();
        let guessed_padding = guess_prefix_size(|input| {
            let mut data = random_bytes(padding_size);
            data.extend_from_slice(input);
            let data = pad(&data, BLOCK_SIZE);

            ecb::encrypt(&data, &key)
        }, BLOCK_SIZE);

        assert_eq!(guessed_padding.unwrap(), padding_size);
    }

    #[test]
    fn test_guess_padding_size_not_matching_block_size() {
        let padding_size = 11;
        let key = random_key();
        let guessed_padding = guess_prefix_size(|input| {
            let mut data = random_bytes(padding_size);
            data.extend_from_slice(input);
            let data = pad(&data, BLOCK_SIZE);

            ecb::encrypt(&data, &key)
        }, BLOCK_SIZE);

        assert_eq!(guessed_padding.unwrap(), padding_size);
    }

    #[test]
    fn test_guess_padding_size_not_matching_block_size_large() {
        let padding_size = 24;
        let key = random_key();
        let guessed_padding = guess_prefix_size(|input| {
            let mut data = random_bytes(padding_size);
            data.extend_from_slice(input);
            let data = pad(&data, BLOCK_SIZE);

            ecb::encrypt(&data, &key)
        }, BLOCK_SIZE);

        assert_eq!(guessed_padding.unwrap(), padding_size);
    }

    #[test]
    fn test_guess_padding_size_zero() {
        let key = random_key();
        let guessed_padding = guess_prefix_size(|input| {
            let data = pad(&input.to_vec(), BLOCK_SIZE);

            ecb::encrypt(&data, &key)
        }, BLOCK_SIZE);

        assert_eq!(guessed_padding.unwrap(), 0);
    }

    #[test]
    fn test_guess_padding_size() {
        let mut rng = thread_rng();
        let padding_size = rng.gen_range(1, BLOCK_SIZE*2);
        let key = random_key();
        let guessed_padding = guess_prefix_size(|input| {
            let mut data = random_bytes(padding_size);
            data.extend_from_slice(input);
            let data = pad(&data, BLOCK_SIZE);

            ecb::encrypt(&data, &key)
        }, BLOCK_SIZE);

        assert_eq!(guessed_padding.unwrap(), padding_size);
    }

    #[test]
    fn test_guess_prefix_size_ecb_with_cbc_oracle() {
        let key = random_key();

        let guess = guess_prefix_size(|input| {
            cbc::encrypt(&pad(&input, BLOCK_SIZE), &key, &key)
        }, BLOCK_SIZE);

        assert!(guess.is_none());
    }

    #[test]
    fn challenge_12() {
        let message = b64::decode("Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK");
        let key = random_key();

        let ans = byte_at_a_time_ecb_with_prefix(|input| {
            let mut data = input.to_vec();
            data.extend_from_slice(&message);
            let data = pad(&data, BLOCK_SIZE);

            ecb::encrypt(&data, &key)
        });

        assert_eq!(ans, message);
    }

    #[test]
    fn challenge_13() {
        let key = random_key();

        let encrypted_admin_role = make_role_admin(|email| {
            encrypted_profile(email, &key)
        });

        let decrypted_bytes = ecb::decrypt(&encrypted_admin_role, &key);
        let profile = url::decode(&String::from_utf8_lossy(unpad(&decrypted_bytes).unwrap()));

        assert_eq!(profile.get("role").unwrap(), "admin");
    }

    #[test]
    fn challenge_14() {
        let message = random_string();
        let key = random_key();
        let prefix = random_count_bytes();

        let ans = byte_at_a_time_ecb_with_prefix(|input| {
            let mut data = prefix.clone();
            data.extend_from_slice(input);
            data.extend(message.bytes());
            let data = pad(&data, BLOCK_SIZE);

            ecb::encrypt(&data, &key)
        });

        assert_eq!(String::from_utf8_lossy(&ans), message);
    }
}
