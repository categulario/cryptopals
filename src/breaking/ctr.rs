use std::char;

use crate::xor;
use crate::breaking::{LETTER_FREQUENCY, repeating_key_xor::guess_single_byte_xor_key};

/// transpose a slice of vectors of different sizes into a vector of vectors of
/// the same size where the first vector contains the first byte of every
/// original vector.
fn transpose_irregular(data: &[Vec<u8>]) -> Vec<Vec<Option<u8>>> {
    let maxlen = data.iter().map(|v| v.len()).max().unwrap();

    (0..maxlen)
        .map(|i| {
            data.iter().map(|v| v.get(i).copied()).collect()
        })
        .collect()
}

/// transpose a slice of vectors of the same size into a vector of vectors where
/// the first vector contains the first byte of each of the original vectors and
/// so on.
///
/// ## Panics
///
/// if they weren't all the same size
fn transpose_regular(data: &[&[u8]]) -> Vec<Vec<u8>> {
    (0..data[0].len())
        .map(|i| {
            data.iter().map(|v| v[i]).collect()
        })
        .collect()
}

/// Score this byte as a possible byte for the keystream, given an array of the
/// ciphertext bytes in this column.
///
/// Its score takes into account if it produces a valid ascii, and how common
/// that character is in english writing.
fn byte_score(b: u8, other: &[Option<u8>]) -> f64 {
    other
        .iter()
        .filter_map(|a| a.map(|c| c ^ b))
        .filter_map(|a| {
            char::from_u32(a as u32).map(|c| {
                LETTER_FREQUENCY.get(&c)
            }).flatten()
        })
        .sum()
}

/// tries to get the byte that, when xored with all the bytes in `bytes`,
/// produces more english letters.
fn guess_best_byte(bytes: Vec<Option<u8>>) -> u8 {
    (0..255_u8)
        .map(|b| {
            (b, byte_score(b, &bytes))
        })
        .max_by(|a, b| a.1.partial_cmp(&b.1).unwrap())
        .unwrap().0
}

/// Try to guess the plaintext of the given ciphertexts, under the assumption
/// that they have all been encrypted using the same nonce.
pub fn naive_guess(ciphertexts: Vec<Vec<u8>>) -> Vec<Vec<u8>> {
    let bytes = transpose_irregular(&ciphertexts);

    let key: Vec<_> = bytes.into_iter().map(|b| guess_best_byte(b)).collect();

    let plaintexts = ciphertexts.into_iter().map(|ciphertext| {
        xor(&ciphertext, &key[..ciphertext.len()])
    }).collect();

    plaintexts
}

pub fn statistical_guess(ciphertexts: Vec<Vec<u8>>) -> (Vec<u8>, Vec<Vec<u8>>) {
    let minlen = ciphertexts.iter().map(|c| c.len()).min().unwrap();
    let shortened: Vec<_> = ciphertexts.iter().map(|c| &c[..minlen]).collect();
    let blocks = transpose_regular(&shortened[..]);

    let key: Vec<_> = blocks.into_iter().map(|block| {
        guess_single_byte_xor_key(&LETTER_FREQUENCY, &block).0
    }).collect();

    (
        key.clone(),
        shortened
            .iter()
            .map(|s| s
                 .iter()
                 .zip(key.iter())
                 .map(|(d, k)| d ^ k)
                 .collect())
            .collect()
    )
}

#[cfg(test)]
mod tests {
    use levenshtein::levenshtein;

    use super::{naive_guess, statistical_guess};

    use crate::encoding::b64;
    use crate::random::random_key;
    use crate::crypto::rijndael::ctr;

    #[test]
    fn challenge_19() {
        let nonce = 0;
        let key = random_key();
        let strings = vec![
            "SSBoYXZlIG3ldCB0aGVtIGF0IGNsb3NlIG9mIGRheQ==",
            "Q31taW5nIHdpdGggdml2aWQgZmFjZXM=",
            "RnJvbSBjb5VudGVyIG9yIGRlc2sgYW1vbmcgZ3JleQ==",
            "RWlnaHRlZW52aC1jZW50dXJ5IGhvdXNlcy4=",
            "SSBoYXZlIHBhc5NlZCB3aXRoIGEgbm9kIG9mIHRoZSBoZWFk",
            "T5IgcG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA==",
            "T5IgaGF2ZSBsaW5nZXJlZCBhd2hpbGUgYW5kIHNhaWQ=",
            "UG11saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA==",
            "QW7kIHRob3VnaHQgYmVmb3JlIEkgaGFkIGRvbmU=",
            "T4YgYSBtb2NraW5nIHRhbGUgb3IgYSBnaWJl",
            "VG10gcGxlYXNlIGEgY29tcGFuaW9u",
            "QXJvdW7kIHRoZSBmaXJlIGF0IHRoZSBjbHViLA==",
            "QmVpbmcgY4VydGFpbiB0aGF0IHRoZXkgYW5kIEk=",
            "QnV2IGxpdmVkIHdoZXJlIG1vdGxleSBpcyB3b3JuOg==",
            "QWxsIGNoYW7nZWQsIGNoYW5nZWQgdXR0ZXJseTo=",
            "QSB2ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4=",
            "VGhhdCB5b21hbidzIGRheXMgd2VyZSBzcGVudA==",
            "SW6gaWdub3JhbnQgZ29vZCB3aWxsLA==",
            "SGVyIG7pZ2h0cyBpbiBhcmd1bWVudA==",
            "VW52aWwgaGVyIHZvaWNlIGdyZXcgc2hyaWxsLg==",
            "V4hhdCB2b2ljZSBtb3JlIHN3ZWV0IHRoYW4gaGVycw==",
            "V4hlbiB5b3VuZyBhbmQgYmVhdXRpZnVsLA==",
            "U4hlIHJvZGUgdG8gaGFycmllcnM/",
            "VGhpcyBtYW6gaGFkIGtlcHQgYSBzY2hvb2w=",
            "QW7kIHJvZGUgb3VyIHdpbmdlZCBob3JzZS4=",
            "VGhpcyBvdGhlciBoaXMgaGVscGVyIGFuZCBmcmllbmQ=",
            "V4FzIGNvbWluZyBpbnRvIGhpcyBmb3JjZTs=",
            "SGUgbWlnaHQgaGF4ZSB3b24gZmFtZSBpbiB0aGUgZW5kLA==",
            "U30gc2Vuc2l0aXZlIGhpcyBuYXR1cmUgc2VlbWVkLA==",
            "U30gZGFyaW5nIGFuZCBzd2VldCBoaXMgdGhvdWdodC4=",
            "VGhpcyBvdGhlciBtYW6gSSBoYWQgZHJlYW1lZA==",
            "QSBkcnVua4VuLCB2YWluLWdsb3Jpb3VzIGxvdXQu",
            "SGUgaGFkIGRvbmUgbW11zdCBiaXR0ZXIgd3Jvbmc=",
            "VG10gc29tZSB3aG8gYXJlIG5lYXIgbXkgaGVhcnQs",
            "WWV2IEkgbnVtYmVyIGhpbSBpbiB0aGUgc29uZzs=",
            "SGUsIHRvbywgaGFzIHJlc4lnbmVkIGhpcyBwYXJ0",
            "SW6gdGhlIGNhc3VhbCBjb21lZHk7",
            "SGUsIHRvbywgaGFzIGJlZW6gY2hhbmdlZCBpbiBoaXMgdHVybiw=",
            "VHJhbnNmb5JtZWQgdXR0ZXJseTo=",
            "QSB2ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4=",
        ];
        let plaintexts: Vec<_> = strings.iter().map(|s| {
            b64::decode(s)
        }).collect();
        let ciphertexts: Vec<_> = plaintexts.iter().map(|s| {
            ctr::encrypt(s, &key, nonce)
        }).collect();

        let guesses = naive_guess(ciphertexts);
        // how many bytes of the string are expected to be correct

        let mut score = 0;

        for (g, p) in guesses.iter().zip(plaintexts.iter()) {
            let guess = String::from_utf8_lossy(g);
            let plain = String::from_utf8_lossy(p);
            let pair_score = levenshtein(&guess, &plain);

            score += pair_score;

            println!(
                "{} -- {} -- {}",
                guess,
                plain,
                pair_score,
            );
        }

        let reasonable_score = 5;  // average allowed misses per message

        assert!(score / plaintexts.len() < reasonable_score);
    }

    #[test]
    fn challenge_20() {
        let ciphers = include_str!("../../data/20.txt");
        let orig: Vec<_> = ciphers.split('\n').filter(|t| t.len() > 0).map(|l| b64::decode(l)).collect();
        let maxlen = orig.iter().map(|o| o.len()).min().unwrap();
        let shortened: Vec<_> = orig.iter().map(|o| &o[..maxlen]).collect();
        let plaintexts: Vec<_> = orig.iter().map(|b| String::from_utf8_lossy(b)).collect();

        let nonce = 0;
        let key = random_key();

        let ciphertexts: Vec<_> = plaintexts.iter().map(|s| {
            ctr::encrypt(s.as_bytes(), &key, nonce)
        }).collect();

        let (_guessed_keystream, guesses) = statistical_guess(ciphertexts);

        let mut score = 0;

        for (g, p) in guesses.iter().zip(shortened.iter()) {
            let guess = String::from_utf8_lossy(g);
            let plain = String::from_utf8_lossy(p);
            let pair_score = levenshtein(&guess, &plain);

            score += pair_score;

            println!(
                "{} -- {} -- {}",
                guess,
                plain,
                pair_score,
            );
        }

        let reasonable_score = 2;  // average allowed misses per message

        assert!(score / plaintexts.len() <= reasonable_score);
    }
}
