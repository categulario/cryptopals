use std::time::SystemTime;

use rand::prelude::*;

use crate::random::mt19937::Generator;

pub fn challenge22_func() -> (u64, u64) {
    let mut rng = thread_rng();

    let mut seed = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs();
    seed -= rng.gen_range(0, 1000);
    let mut gen = Generator::init_genrand(seed);

    (seed, gen.next().unwrap())
}

/// Cracks the seed of an MT19937 given the first integer it generates and a
/// maximum number of seconds since it was seeded from the current timestamp.
pub fn crack_seed_from_first_integer(number: u64, secs_ago: u64) -> Option<u64> {
    let now = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs();

    for i in 0..=secs_ago {
        let seed = now - i;

        if Generator::init_genrand(seed).next().unwrap() == number {
            return Some(seed);
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use super::{crack_seed_from_first_integer, challenge22_func};

    #[test]
    fn test_crack_seed() {
        let (seed, number) = challenge22_func();

        println!("Seed: {}", seed);

        assert_eq!(crack_seed_from_first_integer(number, 2000).unwrap(), seed);
    }
}
