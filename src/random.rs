use rand::{thread_rng, Rng, seq::SliceRandom};

use crate::{BLOCK_SIZE, pad, crypto::{EncryptionMode, rijndael::{cbc, ecb}}};

pub mod mt19937;

/// Generate exactly `how_many` random bytes
pub fn random_bytes(how_many: usize) -> Vec<u8> {
    let mut rng = thread_rng();

    (0..how_many).map(|_i| rng.gen()).collect()
}

/// Generate a random amount of random bytes (between 1 and 99)
pub fn random_count_bytes() -> Vec<u8> {
    let mut rng = thread_rng();

    random_bytes(rng.gen_range(1, 100))
}

/// Generate a random utf8 valid string of length between 1 and 99
pub fn random_string() -> String {
    let mut rng = thread_rng();

    let chars = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'];

    (0..rng.gen_range(1, 100)).map(|_| chars.choose(&mut rng).unwrap()).collect()
}

pub fn random_key() -> Vec<u8> {
    random_bytes(BLOCK_SIZE)
}

pub fn encryption_oracle(data: &[u8]) -> (EncryptionMode, Vec<u8>) {
    let mut rng = thread_rng();
    let key = random_key();
    let left_padding = random_bytes(rng.gen_range(5, 11));
    let right_padding = random_bytes(rng.gen_range(5, 11));

    let mut padded_data = Vec::with_capacity(
        data.len() + left_padding.len() + right_padding.len()
    );

    padded_data.extend(left_padding);
    padded_data.extend(data);
    padded_data.extend(right_padding);

    if rng.gen() {
        (
            EncryptionMode::CBC,
            cbc::encrypt(&pad(&padded_data, BLOCK_SIZE), &key, &random_key())
        )
    } else {
        (
            EncryptionMode::ECB,
            ecb::encrypt(&pad(&padded_data, BLOCK_SIZE), &key)
        )
    }
}
